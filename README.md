# ADVENTURE CHAT BOX GAME

Name: Andy Lien

Project: Previous PROG8110 Assignment

Description: This is a project that was created for PROG8110

## OVERVIEW

This is an application that was made to take users through a "Choose your own adventure". You will begin chatting with a chatbot that will ask you several questions on how you would like to advance. You will respond with your the words that are bolded in the chatbot's response.

## HEROKU

This application is deployed on HEROKU and can be accessed using the link below:

https://info2300assignment3-alien.herokuapp.com/

To begin, simply message "Hi" or anything you wish to enter.

## INSTALLATION

To install this on your local machine

1. Download the repository or clone the repository using the prompts at the top of the Source page
2. Open folder in an editor (I would reccomend VSCode)
3. You will open the terminal and type the following:

```bash
npm install
npm start
```
4. The application should run listening on a port
5. Navigate to any web browser and type in http://localhost:$PORTNUMBER.
6. Begin playing the adventure game!

Please note $PORTNUMBER is the port number that the app is listening on, it will start which port in the terminal after you type

```bash
npm start
```

## License
[MIT](https://choosealicense.com/licenses/mit/)



